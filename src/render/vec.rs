use anyhow::{anyhow, Context, Result};
use glam::{Vec2, Vec3, Vec4};

// TODO Pretty this up somehow
pub fn try_parse_vec2(value: &str) -> Result<Vec2> {
    let mut split = value.split_whitespace();
    let x = parse(split.next()).with_context(|| format!("Unable to parse x in \"{}\"", value))?;
    let y = parse(split.next()).with_context(|| format!("Unable to parse y in \"{}\"", value))?;
    Ok(Vec2::new(x, y))
}

pub fn try_parse_vec3(value: &str) -> Result<Vec3> {
    let mut split = value.split_whitespace();
    let x = parse(split.next()).with_context(|| format!("Unable to parse x in \"{}\"", value))?;
    let y = parse(split.next()).with_context(|| format!("Unable to parse y in \"{}\"", value))?;
    let z = parse(split.next()).with_context(|| format!("Unable to parse z in \"{}\"", value))?;
    Ok(Vec3::new(x, y, z))
}

pub fn try_parse_vec4(value: &str) -> Result<Vec4> {
    let mut split = value.split_whitespace();
    let x = parse(split.next()).with_context(|| format!("Unable to parse x in \"{}\"", value))?;
    let y = parse(split.next()).with_context(|| format!("Unable to parse y in \"{}\"", value))?;
    let z = parse(split.next()).with_context(|| format!("Unable to parse z in \"{}\"", value))?;
    let w = parse(split.next()).with_context(|| format!("Unable to parse w in \"{}\"", value))?;
    Ok(Vec4::new(x, y, z, w))
}

fn parse(value: Option<&str>) -> Result<f32> {
    Ok(value
        .ok_or(anyhow!("Unable to parse point coordinate"))?
        .parse::<f32>()?)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn coord_partial_eq() {
        assert_eq!(Vec2 { x: 1.0, y: 1.0 }, Vec2 { x: 1.0, y: 1.0 });
    }

    #[test]
    fn coord_try_from() {
        assert_eq!(Vec2 { x: 10.0, y: 20.0 }, try_parse_vec2("10 20").unwrap());
    }

    #[test]
    fn coord_display() {
        assert_eq!("[25, 50]", format!("{}", Vec2 { x: 25.0, y: 50.0 }))
    }
}
