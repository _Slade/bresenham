use glam::Vec2;
use screen::Screen;

pub mod algorithm;
pub mod screen;
pub mod vec;

pub trait Render {
    fn draw(&self, points: &Vec<Vec2>, screen: &mut Screen);
}

pub trait RenderLine {
    fn draw_line(&self, screen: &mut Screen, start: &Vec2, end: &Vec2);
}

impl<T> Render for T
where
    T: RenderLine,
{
    fn draw(&self, points: &Vec<Vec2>, screen: &mut Screen) {
        for line in points.chunks(2) {
            match line {
                [start, end] => self.draw_line(screen, start, end),
                _ => panic!("draw_lines called with invalid options"),
            }
        }
    }
}
