use crate::render::screen::Screen;
use glam::{Vec2, Vec4};

pub fn draw_line(screen: &mut Screen, line: Vec4) {
    let mut start = Vec2::from(line);
    let end = Vec2::new(line.z, line.w);
    let dx = (end.x - start.x).abs();
    let sx = if start.x < end.x { 1.0 } else { -1.0 };
    let dy = -(end.y - start.y).abs();
    let sy = if start.y < end.y { 1.0 } else { -1.0 };
    let mut err = dx + dy;
    loop {
        screen.set(start.x as usize, start.y as usize, 1.0);
        if start.x == end.x && start.y == end.y {
            return;
        }
        let e2 = 2.0 * err;
        if e2 >= dy {
            err += dy;
            start.x += sx;
        }
        if e2 <= dx {
            err += dx;
            start.y += sy;
        }
    }
}
