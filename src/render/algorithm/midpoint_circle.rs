use crate::render::screen::Screen;
use glam::Vec3;

pub fn draw_circle(screen: &mut Screen, c: Vec3) {
    let radius = c.z;
    let mut f = 1.0 - radius;
    let mut ddf_x = 1.0;
    let mut ddf_y = -2.0 * radius;
    let mut x = 0.0;
    let mut y = radius;
    let mut draw = |x: f32, y: f32| {
        screen.set(x as usize, y as usize, 1.0);
    };

    draw(c.x, c.y + radius);
    draw(c.x, c.y - radius);
    draw(c.x + radius, c.y);
    draw(c.x - radius, c.y);

    while x < y {
        if f >= 0.0 {
            y -= 1.0;
            ddf_y += 2.0;
            f += ddf_y;
        }
        x += 1.0;
        ddf_x += 2.0;
        f += ddf_x;
        draw(c.x + x, c.y + y);
        draw(c.x - x, c.y + y);
        draw(c.x + x, c.y - y);
        draw(c.x - x, c.y - y);
        draw(c.x + y, c.y + x);
        draw(c.x - y, c.y + x);
        draw(c.x + y, c.y - x);
        draw(c.x - y, c.y - x);
    }
}
