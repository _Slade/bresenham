use crate::render::screen::Screen;
use glam::{Vec2, Vec4};
use std::mem;

pub fn draw_line(screen: &mut Screen, line: Vec4) {
    let (mut x1, mut y1, mut x2, mut y2) = (line.x, line.y, line.z, line.w);
    let steep = (y2 - y1) > (x2 - x1);
    let p = |x: f32, y: f32| {
        if steep {
            Vec2::new(y, x)
        } else {
            Vec2::new(x, y)
        }
    };
    let mut draw = |p: Vec2, val: f32| {
        screen.draw(p.x as usize, p.y as usize, val);
    };

    if steep {
        mem::swap(&mut x1, &mut y1);
        mem::swap(&mut x2, &mut y2);
    }
    if x2 < x1 {
        mem::swap(&mut x1, &mut x2);
        mem::swap(&mut y1, &mut y2);
    }

    let (dx, dy) = (x2 - x1, y2 - y1);
    let gradient = if dx != 0.0 { dy / dx } else { 1.0 };
    let mut intery = y1 + ifract(x1) * gradient;
    let mut draw_endpoint = |point: Vec2| {
        let xend = point.x.round();
        let yend = point.y + gradient * (xend - point.x);
        let xgap = ifract(point.x + 0.5);
        let (px, py) = (xend.trunc(), yend.trunc());
        draw(p(px, py), ifract(yend) * xgap);
        draw(p(px, py + 1.0), yend.fract() * xgap);
        return xend as usize;
    };

    let xstart = draw_endpoint(p(line.x, line.y)) + 1;
    let xend = draw_endpoint(p(line.z, line.w));

    for x in xstart..xend {
        let y = intery.trunc();
        draw(p(x as f32, y), ifract(intery));
        draw(p(x as f32, y + 1.0), intery.fract());
        intery += gradient;
    }
}

fn ifract(x: f32) -> f32 {
    1.0 - x.fract()
}
