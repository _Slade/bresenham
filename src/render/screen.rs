use std::fmt;

pub struct Screen {
    grid: Vec<Vec<f32>>,
}

// TODO Instead of direct access to "pixels," map from a (-1.0, -1.0), (1.0,
// 1.0) coordinate space?
impl Screen {
    pub fn new(x: usize, y: usize) -> Self {
        let mut grid: Vec<Vec<f32>> = Vec::with_capacity(x);
        for _ in 0..x {
            let mut col = Vec::with_capacity(y);
            for _ in 0..y {
                col.push(0.0);
            }
            grid.push(col);
        }
        Screen { grid }
    }

    pub fn x(&self) -> usize {
        self.grid.len()
    }

    pub fn y(&self) -> usize {
        self.grid[0].len()
    }

    pub fn get(&self, x: usize, y: usize) -> f32 {
        self.grid[x][y]
    }

    pub fn set(&mut self, x: usize, y: usize, val: f32) {
        if self.in_bounds(x, y) {
            self.grid[x][y] = val;
        }
    }

    pub fn draw(&mut self, x: usize, y: usize, val: f32) {
        if self.in_bounds(x, y) {
            self.grid[x][y] = self.get(x, y) + val;
        }
    }

    fn in_bounds(&self, x: usize, y: usize) -> bool {
        x < self.grid.len() && y < self.grid[x].len()
    }
}

impl fmt::Display for Screen {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // Top border
        write!(f, "┌")?;
        for _ in 0..self.x() {
            write!(f, "─")?;
        }
        write!(f, "┐\n")?;

        for y in 0..self.y() {
            write!(f, "│")?;
            for x in 0..self.x() {
                write!(f, "{}", get_box_char(self.get(x, y)))?;
            }
            write!(f, "│\n")?;
        }

        write!(f, "└")?;
        for _ in 0..self.x() {
            write!(f, "─")?;
        }
        write!(f, "┘\n")?;
        Ok(())
    }
}

fn get_box_char(darkness: f32) -> char {
    if darkness < 0.2 {
        ' '
    } else if darkness < 0.4 {
        '░'
    } else if darkness < 0.6 {
        '▒'
    } else if darkness < 0.8 {
        '▓'
    } else {
        '█'
    }
}
