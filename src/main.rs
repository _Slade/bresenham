use crate::opts::getopts;
use crate::render::{
    algorithm::{bresenham, midpoint_circle, xiaolinwu},
    screen::Screen,
};
use anyhow::{Context, Result};
use std::env;

mod opts;
mod render;

static USAGE: &str = "\
USAGE:

    bresenham --dimensions 'X Y' [--antialias|-a] [--line|l 'X1 Y1 X2 Y2' ...] [--circle|-c 'X Y R']

OPTIONS:

    --dimensions, -d
        Specify the dimensions of the output drawing.

    --line, -l
        Draw a line using the given points. Takes 4 whole numbers, indicating \
        the starting and ending (x, y) pairs.

    --circle, -c
        Draw a circle. Takes 3 whole numbers, indicating the (x, y) of the \
        center of the circle and its radius.

    --antialias, -a
        Use an anti-aliased drawing algorithm if avaiable.
";

fn main() -> Result<()> {
    let args = env::args().collect::<Vec<String>>();
    let opts =
        getopts(&mut args.iter().map(AsRef::as_ref).collect::<Vec<&str>>()).context(USAGE)?;

    let mut screen = Screen::new(opts.dimensions.x as usize, opts.dimensions.y as usize);

    for line in opts.lines {
        if opts.antialias {
            xiaolinwu::draw_line(&mut screen, line);
        } else {
            bresenham::draw_line(&mut screen, line);
        }
    }

    for circle in opts.circles {
        midpoint_circle::draw_circle(&mut screen, circle);
    }

    println!("{}", screen);

    Ok(())
}
