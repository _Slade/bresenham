# Bresenham Drawing Toy

A toy project for drawing shapes. Currently draws lines or circles and outputs them to the tty.

# Roadmap
- [x] Line drawing via Bresenham's algorithm.
- [x] Anti-aliased line drawing via Xiaolin Wu's algorithm.
    - [ ] Fix bug where it can't draw a line going straight up.
- [x] Circle drawing via the midpoint-circle method.
