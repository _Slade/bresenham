use crate::render::vec::{try_parse_vec2, try_parse_vec3, try_parse_vec4};
use anyhow::{anyhow, ensure, Result};
use glam::{Vec2, Vec3, Vec4};

#[derive(PartialEq, Debug)]
pub struct Opts {
    pub dimensions: Vec2,
    pub lines: Vec<Vec4>,
    pub circles: Vec<Vec3>,
    pub antialias: bool,
}

pub fn getopts(args: &mut Vec<&str>) -> Result<Opts> {
    ensure!(args.len() > 0, "Insufficient arguments");

    let mut iter = args.iter();
    iter.next(); // Skip executable

    let mut dimensions: Option<Vec2> = None;
    let mut lines: Vec<Vec4> = Vec::new();
    let mut circles: Vec<Vec3> = Vec::new();
    let mut antialias = false;

    while let Some(arg) = iter.next() {
        match *arg {
            "--dimensions" | "-d" => {
                if let Some(val) = iter.next() {
                    let d = try_parse_vec2(val)?;
                    ensure!(d.x <= 1000. || d.y <= 1000., "Dimensions too large");
                    ensure!(d.x > 0. || d.y > 0., "Dimensions too small");
                    dimensions = Some(d);
                }
            }
            "--antialias" | "-a" => {
                antialias = true;
            }
            "--line" | "-l" => {
                if let Some(val) = iter.next() {
                    lines.push(try_parse_vec4(val)?);
                }
            }
            "--circle" | "-c" => {
                if let Some(val) = iter.next() {
                    circles.push(try_parse_vec3(val)?);
                }
            }
            _ => eprintln!("Unrecognized argument: {}", arg),
        }
    }

    if let Some(dimensions) = dimensions {
        Ok(Opts {
            dimensions,
            antialias,
            lines,
            circles,
        })
    } else {
        Err(anyhow!("Dimensions not specified"))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_getopt() {
        let mut args = vec![
            "./bresenham",
            "-d",
            "25 25",
            "--antialias",
            "--line",
            "1 1 2 2",
            "--algorithm",
            "bresenham",
        ];
        assert_eq!(
            getopts(&mut args).unwrap(),
            Opts {
                dimensions: Vec2::new(25.0, 25.0),
                antialias: true,
                lines: vec![Vec4::new(1.0, 1.0, 2.0, 2.0)],
                circles: vec![],
            }
        );
    }
}
